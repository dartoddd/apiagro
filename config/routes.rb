Rails.application.routes.draw do
  post 'authenticate', to: 'authentication#authenticate'

  resources :produtos
  get 'produto/:id/imagens', to: 'produtos#produto_imagens'
  get '/frete', to: 'frete#index'

  resources :produto_imagens
  
end
