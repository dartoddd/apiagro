class CreateProdutoImagens < ActiveRecord::Migration[5.1]
  def change
    create_table :produto_imagens do |t|
      t.string :imagem
      t.integer :sequencia
      t.references :produto, foreign_key: true

      t.timestamps
    end
  end
end
