class AddColumnProdutos < ActiveRecord::Migration[5.1]
  def change
    add_column :produtos, :formato, :integer
    add_column :produtos, :peso, :float
    add_column :produtos, :comprimento, :integer
    add_column :produtos, :altura, :integer
    add_column :produtos, :largura, :integer
    add_column :produtos, :diametro, :integer
  end
end