require 'test_helper'

class ProdutoImagensControllerTest < ActionDispatch::IntegrationTest
  setup do
    @produto_imagem = produto_imagens(:one)
  end

  test "should get index" do
    get produto_imagens_url, as: :json
    assert_response :success
  end

  test "should create produto_imagem" do
    assert_difference('ProdutoImagem.count') do
      post produto_imagens_url, params: { produto_imagem: { imagem: @produto_imagem.imagem, produto_id: @produto_imagem.produto_id, sequencia: @produto_imagem.sequencia } }, as: :json
    end

    assert_response 201
  end

  test "should show produto_imagem" do
    get produto_imagem_url(@produto_imagem), as: :json
    assert_response :success
  end

  test "should update produto_imagem" do
    patch produto_imagem_url(@produto_imagem), params: { produto_imagem: { imagem: @produto_imagem.imagem, produto_id: @produto_imagem.produto_id, sequencia: @produto_imagem.sequencia } }, as: :json
    assert_response 200
  end

  test "should destroy produto_imagem" do
    assert_difference('ProdutoImagem.count', -1) do
      delete produto_imagem_url(@produto_imagem), as: :json
    end

    assert_response 204
  end
end
