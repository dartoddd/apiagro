class FreteController < ApplicationController
  skip_before_action :authenticate_request
  before_action :valida_params, only: %i[index]

  SEDEX_VAREJO = 40010.freeze
  PAC_VAREJO = 41106.freeze

  def index
    client = Savon.client wsdl: 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx?WSDL'

    sedex_response = client.call(:calc_preco_prazo, message: parametros(cd_servico: SEDEX_VAREJO, cep_destino: params[:cep_destino], produto: @produto))
    pac_response = client.call(:calc_preco_prazo, message: parametros(cd_servico: PAC_VAREJO, cep_destino: params[:cep_destino], produto: @produto))

    render json: {  sedex: filtra_c_servico(sedex_response.to_hash),
                    pac: filtra_c_servico(pac_response.to_hash) }
  end

  def parametros(cd_servico:, cep_origem: '01202001', cep_destino: '13024045', produto:)
    parametros = {}

    parametros[:nCdEmpresa] = ''
    parametros[:sDsSenha] = ''
    parametros[:sCepOrigem] = cep_origem
    parametros[:sCepDestino] = cep_destino
    parametros[:nVlPeso] = produto[:peso] if produto[:peso]# Kg
    parametros[:nCdFormato] = produto[:formato] if produto[:formato]
    parametros[:nVlComprimento] = produto[:comprimento] if produto[:comprimento]# cm
    parametros[:nVlAltura] = produto[:altura] if produto[:altura]# cm
    parametros[:nVlLargura] = produto[:largura] if produto[:largura] # cm
    parametros[:nVlDiametro] = produto[:diametro] if produto[:diametro]
    parametros[:sCdMaoPropria] = 'n'
    parametros[:nVlValorDeclarado] = 0
    parametros[:sCdAvisoRecebimento] = 'n'
    parametros[:nCdServico] = cd_servico
    parametros[:StrRetorno] = 'popup'
    parametros[:nIndicaCalculo] = 3

    parametros
  end

  def filtra_c_servico(frete)
    frete.select { |key, value| return key == :c_servico ? value : filtra_c_servico(value) }[:c_servico]
  end

  def valida_params
    params_invalidos = []
    params_invalidos << "Necessita do parâmetro #{:produto_id}" if params[:produto_id].blank?
    params_invalidos << "Necessita do parâmetro #{:cep_destino}" if params[:cep_destino].blank?

    @produto = Produto.find_by id: params[:produto_id]
    params_invalidos << "Produto não encontrado #{:cep_destino}" if @produto.blank?

    render json: { erro: 'inputs', inputs: params_invalidos }, status: :bad_request unless params_invalidos.blank?
  end
end
