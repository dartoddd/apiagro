class ProdutoImagensController < ApplicationController
  before_action :set_produto_imagem, only: [:show, :update, :destroy]
  before_action :ajuste_produto_imagem_sequencia, only: [:create, :update]

  # GET /produto_imagens
  def index
    @produto_imagens = ProdutoImagem.all

    render json: @produto_imagens
  end

  # GET /produto_imagens/1
  def show
    render json: @produto_imagem
  end

  # POST /produto_imagens
  def create
    @produto_imagem = ProdutoImagem.new(produto_imagem_params)

    if @produto_imagem.save
      render json: @produto_imagem, status: :created, location: @produto_imagem
    else
      render json: @produto_imagem.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /produto_imagens/1
  def update
    if @produto_imagem.update(produto_imagem_params)
      render json: @produto_imagem
    else
      render json: @produto_imagem.errors, status: :unprocessable_entity
    end
  end

  # DELETE /produto_imagens/1
  def destroy
    @produto_imagem.destroy
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_produto_imagem
    @produto_imagem = ProdutoImagem.find(params[:id])
  end

  def ajuste_produto_imagem_sequencia
    return unless produto_imagem_params[:sequence].nil?

    prodima = ProdutoImagem.where(produto_id: produto_imagem_params[:produto_id]).maximum(:sequencia)
    params.require(:produto_imagem)[:sequencia] = prodima.nil? ? 1 : prodima + 1
  end

  # Only allow a trusted parameter "white list" through.
  def produto_imagem_params
    params.require(:produto_imagem).permit(:imagem, :sequencia, :produto_id)
  end
end
