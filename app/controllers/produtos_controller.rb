class ProdutosController < ApplicationController
  skip_before_action :authenticate_request, only: %i[index show]
  before_action :set_produto, only: %i[show update destroy produto_imagens]

  # GET /produtos
  def index
    @produtos = Produto.all

    render json: @produtos
  end

  # GET /produtos/1
  def show
    render json: @produto
  end

  # GET /produtos/1/imagens
  def produto_imagens
    render json: @produto.produto_imagens
  end

  # POST /produtos
  def create
    @produto = Produto.new(produto_params)

    if @produto.save
      render json: @produto, status: :created, location: @produto
    else
      render json: @produto.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /produtos/1
  def update
    if @produto.update(produto_params)
      render json: @produto
    else
      render json: @produto.errors, status: :unprocessable_entity
    end
  end

  # DELETE /produtos/1
  def destroy
    @produto.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_produto
    @produto = Produto.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def produto_params
    params.require(:produto).permit(:nome, :descricao, :peso, :comprimento, :altura, :largura, :formato, :diametro)
  end
end
