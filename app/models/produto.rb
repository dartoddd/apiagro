class Produto < ApplicationRecord
  FORMATO_CAIXA_PACOTE = 1.freeze
  FORMATO_ROLO_PRISMA = 2.freeze
  FORMATO_ENVELOPE = 3.freeze

  has_many :produto_imagens

  validates :altura, numericality: {only_integer: true, equal_to: 0}, unless: :formato_caixa_pacote?
  validates :diametro, numericality: {only_integer: true, equal_to: 0}, unless: :formato_rolo_prisma?
  validates :formato, numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 3 }
  validates :peso, numericality: { greater_than: 0, less_than_or_equal_to: 30 }

  # CAIXA/PACOTE - 1
  validates :largura, numericality: { only_integer: true, greater_than_or_equal_to: 11, less_than_or_equal_to: 105 }, if: :formato_caixa_pacote?
  validates :comprimento, numericality: { only_integer: true, greater_than_or_equal_to: 16, less_than_or_equal_to: 105  }, if: :formato_caixa_pacote?
  validates :altura, numericality: { only_integer: true, greater_than_or_equal_to: 2, less_than_or_equal_to: 105  }, if: :formato_caixa_pacote?
  validate :soma_compr_larg_alt_menor_200?, if: :formato_caixa_pacote?

  # ROLO/PRISMA - 2
  validates :diametro, numericality: { only_integer: true, greater_than_or_equal_to: 5, less_than_or_equal_to: 91 }, if: :formato_rolo_prisma?
  validates :comprimento, numericality: { only_integer: true, greater_than_or_equal_to: 18, less_than_or_equal_to: 105 }, if: :formato_rolo_prisma?
  validate :soma_compr_dobro_diam_menor_200?, if: :formato_rolo_prisma?

  # ENVELOPE - 3
  validates :peso, numericality: { greater_than: 0, less_than_or_equal_to: 1 }, if: :formato_envelope?
  validates :largura, numericality: { only_integer: true, greater_than_or_equal_to: 11, less_than_or_equal_to: 60 }, if: :formato_envelope?
  validates :comprimento, numericality: { only_integer: true, greater_than_or_equal_to: 16, less_than_or_equal_to: 60  }, if: :formato_envelope?

  def soma_compr_dobro_diam_menor_200?
    raise unless (2 * diametro) + comprimento <= 200
  rescue
    errors.add(:diametro, 'comprimento + (diametro * 2) deve ser menor ou igual a 200')
  end

  def soma_compr_larg_alt_menor_200?
    raise unless comprimento + largura + altura <= 200
  rescue
    errors.add(:altura, 'comprimento + largura + altura deve ser menor ou igual a 200')
  end

  def soma_compr_larg_menor_120?
    raise unless comprimento + largura <= 120
  rescue
    errors.add(:comprimento, 'comprimento + largura deve ser menor ou igual a 120')
  end

  def formato_caixa_pacote?
    formato == FORMATO_CAIXA_PACOTE
  end

  def formato_rolo_prisma?
    formato == FORMATO_ROLO_PRISMA
  end

  def formato_envelope?
    formato == FORMATO_ENVELOPE
  end

end
